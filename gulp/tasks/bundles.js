var glob = require("glob");
var gulp = require("gulp");
var build = require("./build");

var BUNDLE_REGEXP = /(.+\/bundles\/([^/]+)\/)/;

module.exports = function(entry) {

    return function() {
        var bundles = glob.sync(entry + "/bundles/*/index.js");

        bundles.forEach(function(path) {
            var bundlePath = BUNDLE_REGEXP.exec(path),
                bundleName = bundlePath[2],
                destination = "@build/" + bundleName;

            bundlePath = bundlePath[1];

            console.log("Building bundle:", bundleName);

            gulp.src(bundlePath + "/*.{html,css}")
                .pipe(gulp.dest(destination));

            build(path, {dest: destination})();

        });
    };

};
