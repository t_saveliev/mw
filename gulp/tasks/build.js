var gulp = require("gulp");
var browserify = require('gulp-browserify');
var templateTransform = require("../../src/transform/transform");

var basic = {
    transform: [templateTransform()],
    paths: [
        "./src/core",
        "./src/corelib/",
        "./src/",
        "./node_modules"
    ],
    debug: true
};

module.exports = function(entry, opt) {
    opt || (opt = {});

    return function() {
        gulp.src(entry, {read: false})
            .pipe(browserify(basic))
            .pipe(gulp.dest(opt.dest));
    };
};