var createClass = require("createClass");
var Any = require("corelib/components/Any");
var StackCallbackSupport = require("StackCallbackSupport");
var $ = require("jquery");

module.exports = createClass("button", Any, {

    _init: function() {
        this.tag = "button";
    },

    _cleanupRender: function(MW) {
        var _this = this;

        StackCallbackSupport.push(function() {
            $(_this.element).bind("click", function(e) {
                _this.fireEvent("click", _this, e);
            });
        });

        MW.end();
    }

});
