var createClass = require("createClass");
var MarkupRender = require("MarkupRender");
var AbstractComponent = require("AbstractComponent");

module.exports = createClass("body", AbstractComponent, {

    _setupRender: function() {
        MarkupRender.renderComponentBody(this.getContext(), this);
        return false;
    }

});
