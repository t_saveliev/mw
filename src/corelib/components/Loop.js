var createClass = require("createClass");
var AbstractComponent = require("AbstractComponent");

module.exports = createClass("loop", AbstractComponent, {

    parameters: {
        source: {required: true},
        index: {defaults: 0},
        value: {}
    },

    _setupRender: function() {
        this.sourceLength = this.source.length - 1;
        this.index.set(0);
    },

    _beforeRender: function() {
        this.value && this.value.set(this.source[this.index.get()]);
    },

    _afterRender: function() {
        var i = this.index.get();

        if (i < this.sourceLength) {
            this.index.set(i + 1);

            return false;
        }
    }
});
