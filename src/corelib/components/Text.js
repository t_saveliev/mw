var createClass = require("createClass");
var AbstractComponent = require("AbstractComponent");

module.exports = createClass("text", AbstractComponent, {

    parameters: {
        text: {required: true}
    },

    _setupRender: function(MW) {
        this.element = MW.writeText(this.text);

        return false;
    },

    update: function() {
        this.element && (this.element.textContent = this.read("text"));
    }

});
