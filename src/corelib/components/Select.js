var createClass = require("createClass");
var Any = require("corelib/components/Any");
var StackCallbackSupport = require("StackCallbackSupport");
var $ = require("jquery");

module.exports = createClass("select", Any, {

    parameters: {
        source: {required: true},
        value: {}
    },

    _init: function() {
        this.sourceLength = this.source.length - 1;
        this.index = 0;
    },

    _setupRender: function(MW) {
        this.element = MW.write("select");
        MW.applyInformalParameters();
    },

    _beforeRender: function(MW) {
        MW.write("option");
        MW.writeText(this.source[this.index]);
        MW.end();

        return false;
    },

    _afterRender: function() {
        if (this.index < this.sourceLength) {
            this.index++;

            return false;
        }
    },

    _cleanupRender: function(MW) {
        var _this = this,
            element = this.element;

        StackCallbackSupport.push(function() {
            $(element).bind("change", function(e) {
                _this.fireEvent("change", _this, element.value, e);
            });
        });

        this.value && this.setValue();
        MW.end();
    },

    setValue: function(value) {
        value && this.value.set(value);
        this.element.value = value || this.value.get();
    },

    getValue: function() {
        return this.value;
    }

});
