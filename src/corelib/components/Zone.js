var createClass = require("createClass");
var MarkupRender = require("MarkupRender");
var Any = require("corelib/components/Any");

module.exports = createClass("zone", Any, {

    updateZone: function(block) {
        this.clearExternalResources();

        var element = this.element,
            renderResult;

        element.textContent = "";

        renderResult = MarkupRender.renderComponentBody(this, this, block);

        element.appendChild(renderResult);
    }

});
