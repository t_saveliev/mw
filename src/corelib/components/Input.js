var createClass = require("createClass");
var Any = require("corelib/components/Any");
var StackCallbackSupport = require("StackCallbackSupport");
var $ = require("jquery");

var ENTER_KEY_CODE = 13;

module.exports = createClass("input", Any, {

    parameters: {
        type: {},
        value: {},
        checked: {}
    },

    _setupRender: function(MW) {
        var element = this.element = MW.write("input"),
            type = this.type || "text",
            _this = this;

        element.setAttribute("type", type);

        switch (type) {
            case "checkbox" :
                if (this.checked != null) {
                    element.checked = this.checked;
                }
                break;
        }

        MW.applyInformalParameters();

        StackCallbackSupport.push(function() {
            $(element)
                .bind("input", _this._onChange.bind(_this))
                .bind("keydown", _this._onKeyDown.bind(_this))
                .bind("change", _this._toggle.bind(_this))
                .bind("blur", _this._save.bind(_this));
        });

        return false;
    },

    setValue: function(value) {
        this.value = value;
        this.element.value = value;
    },

    _onChange: function(e) {
        this.value = e.target.value;
    },

    _save: function() {
        this.fireEvent("save", this, this.value);
    },

    _toggle: function() {
        this.fireEvent("toggle", this, this.value);
    },

    _onKeyDown: function(e) {
        if (e.keyCode === ENTER_KEY_CODE) {
            this._save();
        }
    }

});
