var createClass = require("createClass");
var MarkupRender = require("MarkupRender");
var AbstractComponent = require("AbstractComponent");
var Block = require("Block");

module.exports = createClass("delegate", AbstractComponent, {

    parameters: {
        to:  {type: Block, required: true}
    },

    _setupRender: function() {
        MarkupRender.renderComponentBody(this, this, this.to);
        return false;
    }

});
