var createClass = require("createClass");
var MarkupRender = require("MarkupRender");
var AbstractComponent = require("AbstractComponent");
var PropertyExpression = require("PropertyExpression");

module.exports = createClass("any", AbstractComponent, {

    parameters: {
        tag: {defaults: "div"}
    },

    _destroy: function() {
        var element = this.element;

        if (element.parentNode) {
            element.parentNode.removeChild(element);
        }
    },

    _setupRender: function(MW) {
        this.element = MW.write(this.tag);
        MW.applyInformalParameters();
    },

    _cleanupRender: function(MW) {
        MW.end();
    },

    update: function() {
        this.clearExternalResources();
        this.clearInternalResources();

        var element = this.element,
            parentNode = element.parentNode,
            renderResult;

        if (parentNode == null) return false;

        renderResult = this.render();

        parentNode.insertBefore(renderResult, element);
        parentNode.removeChild(element);

        return true;
    },

    updateBody: function() {
        this.clearExternalResources();

        var element = this.element,
            renderResult;

        renderResult = MarkupRender.renderComponentBody(this);

        element.textContent = "";
        element.insertBefore(renderResult, null);

        return true;
    },

    updateAttr: function(attr) {
        var informalParameters = this._informalParameters,
            _prop;

        if (_prop = informalParameters[attr]) {
            this.setAttr(attr, PropertyExpression.parse(_prop, this.getContext(), this.getContainer()));
        }
    },

    setAttr: function(name, value) {
        this.element.setAttribute(name, value);
    },

    getAttr: function(name) {
        this.element.getAttribute(name);
    }

});
