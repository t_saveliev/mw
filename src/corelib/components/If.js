var createClass = require("createClass");
var AbstractComponent = require("AbstractComponent");
var Block = require("Block");

module.exports = createClass("if", AbstractComponent, {

    parameters: {
        test: {required: true},
        else: {type: Block, create: true}
    },

    _setupRender: function() {
        if (!this.test) {
            if (this.else) {
                return this.else;
            }
            return false;
        }
    }

});
