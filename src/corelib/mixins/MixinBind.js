var createClass = require("createClass");
var AbstractMixin = require("AbstractMixin");
var StackCallbackSupport = require("StackCallbackSupport");
var Any = require("corelib/components/Any");

var bindingFormat = /^([\s\S]+):from:([^\:][\w.]*)$|^([\s\S]+[^:]$)/;

function parseBindingExpression(expression, componentContext, callback) {
    var arr = expression.split(","),
        i = 0,
        len = arr.length;

    for (; i < len; i++) {
        var matches = bindingFormat.exec(arr[i].trim());

        if (matches) {
            /*
            *   если выражение содержит объект наблюдения указанный :from:
            */

            if (matches[2]) {
                //console.log(componentContext.get(matches[2]), matches[1]);
            } else if (matches[3]) {
                /*
                *   устанавливаем слушателя для контекстного компонента
                */
                this.listenTo(componentContext, matches[3], callback);
            }
        }
    }
}

module.exports = createClass("bind", AbstractMixin, {

    parameters: {
        body: {},
        attr: {},
        update: {}
    },

    _cleanupRender: function(MW) {

        this.stopListening();

        this._currentComponent || (this._currentComponent = MW.getCurrentComponent());

        var currentComponent = this._currentComponent,
            componentContext = currentComponent.getContext();

        this.update && typeof currentComponent.update === "function" && StackCallbackSupport.push(function() {
            this.bindUpdateComponent(componentContext);
        }, this);

        if (!(currentComponent instanceof Any)) return;

        this.body && StackCallbackSupport.push(function() {
            this.bindUpdateBody(componentContext);
        }, this);

        this.attr && StackCallbackSupport.push(function() {
            this.bindUpdateAttributes(componentContext);
        }, this);
    },

    bindUpdateBody: function(componentContext) {
        parseBindingExpression.call(this, this.body, componentContext, (function() {
            return function() {
                this._currentComponent.updateBody();
            };
        })());
    },

    bindUpdateAttributes: function(componentContext) {
        var attributes = this.attr,
            attr;

        for (attr in attributes) {
            parseBindingExpression.call(this, attributes[attr], componentContext, (function() {
                return function() {
                    this._currentComponent.updateAttr(attr);
                };
            })());
        }
    },

    bindUpdateComponent: function(componentContext) {
        parseBindingExpression.call(this, this.update, componentContext, (function() {
            return function() {
                this._currentComponent.update();
            };
        })());
    }

});
