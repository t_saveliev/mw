var createClass = require("createClass");

module.exports = createClass("block", {}, {

    get: function(expression, componentContext) {
        return componentContext.getBlock(expression);
    }

});
