var createClass = require("createClass");
var PropertyExpression = require("PropertyExpression");
var Property = require("Property");

var descriptionFormat = /([\w.]+)(\((.+)?\))?/;

function getVariableFromComponent(ctx, key) {
    var asp = key.split("."),
        path = ctx,
        len = asp.length - 1,
        i = 0;

    for (; i <= len; i++) {
        path = path[asp[i]];
        if (i !== len && path instanceof Property) {
            path = path.get();
        }
        if (path === undefined) return;
    }
    return path;
}

module.exports = createClass("prop", {}, {

    get: function(expression, componentContext, componentContainer) {

        var matches = expression.match(descriptionFormat),
            value, attrs, args;

        if (!matches) return null;

        value = getVariableFromComponent(componentContext, matches[1]);

        if (typeof value === "function" && matches[2]) {
            args = [];

            if (matches[3]) {
                attrs = matches[3].split(",");
                for (var i = 0, len = attrs.length; i < len; i++ ) {
                    args.push(PropertyExpression.get(attrs[i], componentContext, componentContainer));
                }
            }

            return value.apply(componentContext, args);
        }

        return value;
    },

    set: function(expression, componentContext) {

        var matches = expression.match(descriptionFormat);

        if (!matches || matches[2]) return null;

        //componentContext.set(matches[1]);
    }

});
