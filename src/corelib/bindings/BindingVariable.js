var createClass = require("createClass");
var Property = require("Property");

module.exports = createClass("var", {}, {

    get: function(expression, componentContext) {

        var _cache = componentContext._cache || (componentContext._cache = {});

        if (_cache[expression]) {
            return _cache[expression];
        }

        return _cache[expression] = new Property(null);
    }

});
