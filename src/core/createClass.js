var inherit = require("inherit");

function createClass(className) {
    var args = Array.prototype.slice.call(arguments, 1);

    var Constructor = inherit.apply(this, args);
    Constructor.__name = className;

    return Constructor;
}

module.exports = createClass;