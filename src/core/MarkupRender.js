var Block = require("Block");
var PropertyExpression = require("PropertyExpression");
var StackCallbackSupport = require("StackCallbackSupport");
var coreResources = require("coreResources");
var utils = require("core/utils");

var COMPONENTS = coreResources.components,
    MIXINS = coreResources.mixins,

    renderInProgress = false,

    _rootElement = document.createDocumentFragment(),

    _currentElement = _rootElement,
    _componentContext,
    _componentContainer;


"use strict";

function Renderable() {}
Renderable.prototype.render = function() {};

function RenderableComponent(component, props, body) {

    this.props = props || {};
    this.body = new Block(body);

    if (utils.isFunction(component)) {
        this.Component = component;
    } else {
        if (COMPONENTS[component]) {
            this.Component = COMPONENTS[component];
        } else {
            if (!(this.Component = COMPONENTS["any"])) throw new Error();
            this.props.tag = component;
        }
    }
}
RenderableComponent.prototype = Object.create(Renderable.prototype);
RenderableComponent.prototype.render = function() {
    var component;

    component = new (this.Component)(
        this.props,
        this.body,
        _componentContext,
        _componentContainer,
        this.mixins
    );

    component.render();
};
RenderableComponent.prototype.mixin = function(name, props) {
    var mixin;

    if (!(mixin = MIXINS[name])) throw new Error();

    (this.mixins || (this.mixins = [])).push({
        Mixin: mixin,
        props: props
    });

    return this;
};
RenderableComponent.prototype.parameter = function(name, value) {
    this.props[name] = value;

    return this;
};

function RenderableBlock(block) {
    this.block = block;
}
RenderableBlock.prototype = Object.create(Renderable.prototype);
RenderableBlock.prototype.render = function() {
    renderBlock(_componentContext, PropertyExpression.parse(this.block, _componentContext, _componentContainer));
};

function RenderableBody() {}
RenderableBody.prototype = Object.create(Renderable.prototype);
RenderableBody.prototype.render = function() {
    MarkupRender.renderComponentBody(_componentContext, _componentContainer);
};

function decorateRenderFn(_provider, afterFn, beforeFn, callback) {
    return function renderPhase() {
        var res, afterFnRes;

        if ((afterFnRes = afterFn.call(_provider, MarkupWriter)) !== false) {
            if (afterFnRes instanceof Block) {
                res = renderBlock(_provider, afterFnRes);
            } else {
                res = callback.call(MarkupWriter);
            }
        }
        if (beforeFn.call(_provider, MarkupWriter) === false) {
            renderPhase();
        }

        return res;
    };
}
function decorateRenderMixinFn(_provider, fnName) {
    var mixins = _provider.__mixins;

    if (mixins) {
        return function() {
            var res;

            for (var i = 0, len = mixins.length; i < len; i++) {
                res = mixins[i][fnName](MarkupWriter);

                if (res === false) return false;
            }

            return _provider[fnName].call(_provider, MarkupWriter);
        };
    } else {
        return _provider[fnName];
    }
}
function renderBlock(_provider, block) {
    if (typeof block === "string") {
        block = _provider.getBlock(block);
    }

    if (block instanceof Block) {
        return MarkupRender.render(block);
    }
}

function MarkupBuilder() {}
MarkupBuilder.create = function(name, props, body) {
    return new RenderableComponent(name, props, body);
};
MarkupBuilder.renderBody = function() {
    return new RenderableBody();
};
MarkupBuilder.renderBlock = function(block) {
    return new RenderableBlock(block);
};

function MarkupWriter() {}
MarkupWriter.write = function(tagName) {
    var elem = document.createElement(tagName);

    _currentElement.insertBefore(elem, null);
    _currentElement = elem;

    return elem;
};
MarkupWriter.end = function() {
    _currentElement = _currentElement.parentNode;
};
MarkupWriter.writeText = function(string) {
    var element = document.createTextNode(string != null ? string.toString() : "");
    _currentElement.appendChild(element);
    return element;
};
MarkupWriter.getCurrentElement = function() {
  return _currentElement;
};
MarkupWriter.getCurrentComponent = function() {
    return _componentContainer;
};
MarkupWriter.applyInformalParameters = function(element) {
    if (!_componentContainer) return;

    var informalParameters = _componentContainer._informalParameters,
        prop, _prop;

    element = MarkupWriter.getCurrentElement();

    for (prop in informalParameters) {
        _prop = informalParameters[prop];

        element.setAttribute(prop, PropertyExpression.parse(_prop, _componentContainer.getContext(), _componentContainer));
    }
};

function MarkupRender() {}
MarkupRender.buildTemplate = function(template) {
    var blocks = {},
        exports = {},
        block;

    template(exports, MarkupBuilder);

    for (block in exports) {
        blocks[block] = new Block(exports[block]);
    }

    return blocks;
};
MarkupRender.render = function(template) {
    function renderElement(element) {
        if (!element) return;

        if (element instanceof Renderable) {
            element.render();
        } else {
            MarkupWriter.writeText(PropertyExpression.parse(
                (typeof element === "string") ? element : element.toString(),
                _componentContext, _componentContainer)
            );
        }
    }
    function walker(template) {
        var root = _currentElement;

        if (Object.prototype.toString.call(template) === "[object Array]") {
            for (var i = 0, len = template.length; i < len; i++) {
                renderElement(template[i]);
            }
        } else {
            renderElement(template);
        }

        if (root !== _currentElement) throw new Error("element __" + _currentElement + "__ is not closed");
    }

    var isParentRender = false,
        result;

    if (renderInProgress === false) {
        renderInProgress = true;
        isParentRender = true;
    }

    if (template instanceof Block) {
        walker(template.get());
        result = _currentElement;
    }

    if (isParentRender === true) {
        renderInProgress = false;

        // console.log("rendering completed")
        StackCallbackSupport.callAll();
    }

    return result;
};
MarkupRender.renderComponent = function(component) {

    var savedComponentContainer = _componentContainer,
        savedComponentContext = _componentContext;

    _componentContainer = component;
    _componentContext = component;

    var res = decorateRenderFn(component,
        decorateRenderMixinFn(component, "_setupRender"),
        decorateRenderMixinFn(component, "_cleanupRender"),

        decorateRenderFn(component,
            decorateRenderMixinFn(component, "_beforeRender"),
            decorateRenderMixinFn(component, "_afterRender"),

            function() {

                return renderBlock(component, "main");
            }
        )
    )();

    _componentContainer = savedComponentContainer;
    _componentContext = savedComponentContext;

    return res;
};
MarkupRender.renderComponentBody = function(component, container, block) {

    var savedComponentContainer = _componentContainer,
        savedComponentContext = _componentContext;

    _componentContainer = container || component;

    var res = decorateRenderFn(component,
        decorateRenderMixinFn(component, "_beforeRenderBody"),
        decorateRenderMixinFn(component, "_afterRenderBody"),

        function() {
            _componentContext = component.__componentContext;

            var res = renderBlock(component, block || component.__body);

            _componentContext = savedComponentContext;

            return res;
        }
    )();

    _componentContainer = savedComponentContainer;

    return res;
};

module.exports = MarkupRender;
