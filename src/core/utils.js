var idCounter = 0;

var toStr = Object.prototype.toString;

function isFunction(obj) {
    return toStr.call(obj) === '[object Function]';
}

module.exports = {

    getValue: function getValue(source, key) {
        var asp = key.split("."),
            path = source,
            len = asp.length - 1,
            name = asp[len],
            i = 0;

        for (; i < len; i++) {
            path = path[asp[i]];
            if (path === undefined) return;
        }

        return path[name];
    },

    setValue: function setValue(source, key, value) {
        var arr = key.split(/\./),
            len = arr.length,
            path = source,
            i = 0,
            dirName;

        if (len > 0) {
            while (i < len) {
                dirName = arr[i++];
                if (i === len) {
                    path[dirName] = value;
                } else {
                    path = path[dirName] || (path[dirName] = {});
                }
            }
        }
        return source;
    },

    regExpEscape: function regExpEscape(str) {
        return String(str).replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, '\\$&');
    },

    uniqueId: function uniqueId(prefix) {
        var id = ++idCounter + "";
        return prefix ? prefix + id : id;
    },

    toCamelCase: function toCamelCase(string) {
        var splitter = /(^|[:-])(\w)/gi;

        return string.replace(splitter, function(match, prefix, name) {
            return name.toUpperCase();
        });
    },

    isFunction: isFunction

};
