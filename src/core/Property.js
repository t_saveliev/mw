function Property(val) {
    if (!(this instanceof Property)) {
        return new Property(val);
    }
    this.value = val;
}

Property.prototype.set = function(val) {
    return this.value = val;
};

Property.prototype.get = function() {
    if (typeof this.value === "function") {
        return this.value();
    }
    return this.value;
};

Property.prototype.valueOf = function() {
    return this.get();
};

Property.prototype.toString = function() {
    var value = this.get();
    return value ? value.toString() : "";
};

module.exports = Property;