function Block(template) {
    this.set(template || null);
}

Block.prototype.set = function(tempalte) {
    if (tempalte && Object.prototype.toString.call(tempalte) !== "[object Array]") {
        tempalte = [tempalte];
    }

    return this.tml = tempalte;
};

Block.prototype.get = function() {
    return this.tml;
};

module.exports = Block;