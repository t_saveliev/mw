var hStack = [], // "H" - high priority stack
    mStack = [], // "M" - middle priority stack
    lStack = []; // "L" - low priority stack

var flowCallReady = true;

function pushToStack(stack, method, ctx) {
    if (typeof method === "function") {
        stack.push(ctx ? method.bind(ctx) : method);
    }
}

function callStackMethods(stack) {
    var i = stack.length - 1;

    for (; i >= 0; i--) {
        stack[i]();
    }
}

function push(method, ctx, stackName) {
    switch (stackName || "M") {
        case "H" : pushToStack(hStack, method, ctx);break;
        case "M" : pushToStack(mStack, method, ctx);break;
        case "L" : pushToStack(lStack, method, ctx);break;
        default  : throw new Error("unknown stack");
    }
}

function callStack(stackName) {
    switch (stackName) {
        case "H" : callStackMethods(hStack);hStack = [];break;
        case "M" : callStackMethods(mStack);mStack = [];break;
        case "L" : callStackMethods(lStack);lStack = [];break;
        default  : throw new Error("unknown stack");
    }
}

function callAll() {
    if (flowCallReady === false) return;

    flowCallReady = false;
    setTimeout(function() {
        hStack.length && callStack("H");
        mStack.length && callStack("M");
        lStack.length && callStack("L");
        flowCallReady = true;
    }, 0);
}

module.exports = {
    push: push,
    callAll: callAll
};