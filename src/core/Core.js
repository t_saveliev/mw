var MarkupRender = require("MarkupRender");
var createClass = require("createClass");
var coreResources = require("coreResources");

function registerResource(destination, name, resource) {
    if (!coreResources[destination] || !name) {
        throw new Error("Unknown resource destination");
    }

    if (coreResources[destination][name]) {
        throw new Error("Resource by name __" + name + "__ is already registered");
    }

    return coreResources[destination][name] = resource;
}

var Core = {
    createClass         : createClass,
    registerResource    : registerResource,
    buildTemplate       : MarkupRender.buildTemplate,
    resources           : coreResources
};

coreResources.bindings.block        = require("corelib/bindings/BindingBlock");
coreResources.bindings.prop         = require("corelib/bindings/BindingProp");
coreResources.bindings.var          = require("corelib/bindings/BindingVariable");

coreResources.mixins.bind           = require("corelib/mixins/MixinBind");

coreResources.components.any        = require("corelib/components/Any");
coreResources.components.body       = require("corelib/components/Body");
coreResources.components.button     = require("corelib/components/Button");
coreResources.components.delegate   = require("corelib/components/Delegate");
coreResources.components.if         = require("corelib/components/If");
coreResources.components.input      = require("corelib/components/Input");
coreResources.components.loop       = require("corelib/components/Loop");
coreResources.components.select     = require("corelib/components/Select");
coreResources.components.text       = require("corelib/components/Text");
coreResources.components.zone       = require("corelib/components/Zone");

module.exports = Core;