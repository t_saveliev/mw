var MarkupRender = require("MarkupRender");
var Parameter = require("Parameter");
var utils = require("core/utils");

var slice = Array.prototype.slice;

var reservedParameters = {
    resourceId: function(value) {
        this.__resourceId = value;
    }
};

//var Events = require("./EventsEmmiter");
//var inherits = require("inherits");
//inherits(AbstractComponent, Events);
var _ = require("lodash");
_.extend(AbstractComponent.prototype, require("./Events"));
function AbstractComponent(parameters, body, componentContext, componentContainer, mixins) {
    parameters || (parameters = {});

    var parametersConfig = this.parameters,
        pName;

    this.__id = utils.uniqueId("id_");
    this.__name = name || this.__self.__name;
    this.__componentContext = componentContext || null;
    this.__componentContainer = componentContainer || null;
    this.__body = body;

    // init declare parameters
    this._inputParameters = {};
    for (pName in parametersConfig) {
        Parameter.init(this, pName, parameters[pName], parametersConfig[pName]);
    }

    // init informal parameters
    this._informalParameters = {};
    for (pName in parameters) {
        if (reservedParameters.hasOwnProperty(pName)) {
            var method = reservedParameters[pName];
            if (typeof method === "function") method.call(this, parameters[pName]);
        } else {
            if (!parametersConfig || !parametersConfig.hasOwnProperty(pName)) {
                this._informalParameters[pName] = parameters[pName];
            }
        }
    }

    // init mixins
    if (mixins) {
        var _mixins = [],
            _mixin;

        for (var i = 0, len = mixins.length; i < len; i++) {
            _mixin = mixins[i];
            _mixins.push(new (_mixin.Mixin)(_mixin.props));
        }

        this.__mixins = _mixins;
    }

    if (componentContext) componentContext.addToEmbeddedResources(this);
    if (componentContainer) componentContainer.addToComponentResources(this);

    // user init call
    this._init();
}

AbstractComponent.prototype.destruct = function() {
    this.clearExternalResources();
    this.clearInternalResources();

    this.stopListening();

    if (this.__mixins) {
        for (var _mixins = this.__mixins, i = _mixins.length - 1; i >= 0; i--) {
            _mixins[i].destruct();
        }
    }

    this.__componentContext && this.__componentContext.removeFromEmbeddedResources(this);
    this.__componentContainer && this.__componentContainer.removeFromComponentResources(this);

    this._destroy();
};

AbstractComponent.prototype.addToResources = function(component, resourceList) {
    resourceList.push(component);
};
AbstractComponent.prototype.addToEmbeddedResources = function(component) {
    var resourceId = component.__resourceId;

    if (!resourceId) return;
    this.addToResources(component, this._embeddedResources || (this._embeddedResources = []));
};
AbstractComponent.prototype.addToComponentResources = function(component) {
    if (component.__componentContext === this) {
        this.addToResources(component, this._internalResources || (this._internalResources = []));
    } else {
        this.addToResources(component, this._externalResources || (this._externalResources = []));
    }
};

AbstractComponent.prototype.removeFromResources = function(component, resourceList) {
    if (!resourceList) return;
    var index = resourceList.indexOf(component);

    if (index !== -1) {
        resourceList[index] = null;
    }
};
AbstractComponent.prototype.removeFromEmbeddedResources = function(component) {
    this._embeddedResources && this.removeFromResources(component, this._embeddedResources);
};
AbstractComponent.prototype.removeFromComponentResources = function(component) {
    if (component.__componentContext === this) {
        this._internalResources && this.removeFromResources(component, this._internalResources);
    } else {
        this._externalResources && this.removeFromResources(component, this._externalResources);
    }
};

AbstractComponent.prototype.clearResources = function(resourceList) {
    if (!resourceList) return;
    var len = resourceList.length,
        component,
        i = 0;

    for(; i < len; i++ ) {
        component = resourceList[i];

        if (component !== null) {
            component.destruct();
        }
    }
};
AbstractComponent.prototype.clearExternalResources = function() {
    if (this._externalResources) {
        this.clearResources(this._externalResources);
        delete this._externalResources;
    }
};
AbstractComponent.prototype.clearInternalResources = function() {
    if (this._internalResources) {
        this.clearResources(this._internalResources);
        delete this._internalResources;
    }
};

AbstractComponent.prototype.findEmbeddedResource = function(resourceId) {
    if (!this._embeddedResources) return;

    var _embeddedResources = this._embeddedResources,
        len = _embeddedResources.length,
        i = 0,
        isArrayCreated = false,
        component,
        res;

    for (; i < len; i++) {
        component = _embeddedResources[i];
        if (component && component.__resourceId === resourceId) {
            if (res) {
                !isArrayCreated && (isArrayCreated = true) && (res = [res]);
                res.push(component);
            } else {
                res = component;
            }
        }
    }

    return res;
};

AbstractComponent.prototype.triggerMethod = function(event) {
    var methodName = "on" + utils.toCamelCase(event),
        method = this[methodName],
        res;

    if (typeof method === "function") {
        res = method.apply(this, slice.call(arguments, 1));
    }

    if (res !== false && typeof this.trigger === "function") {
        this.trigger.apply(this, arguments);
    }

    return res;
};
AbstractComponent.prototype.fireEvent = function(eventName) {
    var componentContext = this.__componentContext,
        _eventName = eventName;

    if (componentContext) {
        var args = arguments;

        args[0] = _eventName + ":" + this.__self.__name;
        componentContext.triggerMethod.apply(componentContext, args);

        if (this.__resourceId) {
            args[0] = _eventName + ":from:" + this.__resourceId;
            componentContext.triggerMethod.apply(componentContext, args);
        }
    }
};

AbstractComponent.prototype.getContext = function() {
    return this.__componentContext;
};
AbstractComponent.prototype.getContainer = function() {
    return this.__componentContainer;
};
AbstractComponent.prototype.getBlock = function(name) {
    return this.__template[name];
};

AbstractComponent.prototype.read = function(prop) {
    return Parameter.read(this, prop);
};

AbstractComponent.prototype.render = function() {
    return MarkupRender.renderComponent(this);
};

AbstractComponent.prototype._init = function() {};
AbstractComponent.prototype._destroy = function() {};
AbstractComponent.prototype._setupRender = function() {};
AbstractComponent.prototype._beforeRender = function() {};
AbstractComponent.prototype._beforeRenderBody = function() {};
AbstractComponent.prototype._afterRenderBody = function() {};
AbstractComponent.prototype._afterRender = function() {};
AbstractComponent.prototype._cleanupRender = function() {};

AbstractComponent.prototype.__template = MarkupRender.buildTemplate(function(exports, MW) {
    exports.main = [
        MW.renderBody()
    ];
});

module.exports = AbstractComponent;