var coreResources = require("coreResources");
var utils = require("utils");

var BINDINGS = coreResources.bindings,

    openSymbol = "${",
    closeSymbol = "}",

    stringExpression = new RegExp(utils.regExpEscape(openSymbol) + "((?:.|[\r\n])+?)(?:" + utils.regExpEscape(closeSymbol) + "|$)"),

    bindingRegExp = new RegExp("^(\\w+):(.+)");

function parsePropertyExpression(expression, componentContext, componentContainer) {
    var matches = expression.split(stringExpression),
        len = matches.length,
        buffer, text, value;

    if (len === 3 && matches[0] === "" && matches[2] === "") {
        return getPropertyComponentFromExpression(matches[1], componentContext, componentContainer);
    }

    buffer = "";
    for (var i = 0; i < len; i++) {
        text = matches[i];

        if (i % 2 === 1) {
            if ((value = getPropertyComponentFromExpression(text, componentContext, componentContainer)) !== void 0) {
                text = value.toString();
            }
        }

        buffer += text;
    }

    return buffer;
}
function getPropertyComponentFromExpression(expression, componentContext, componentContainer) {
    var matches = expression.match(bindingRegExp);

    if (matches) {
        if (!BINDINGS.hasOwnProperty(matches[1])) {
            throw new Error("Unknown binding method __" + matches[1] + "__ in expression __" + expression + "__");
        }
        return (BINDINGS[matches[1]]).get(matches[2], componentContext, componentContainer);
    }
    return BINDINGS["prop"].get(expression, componentContext, componentContainer);
}

function PropertyExpression(expression, componentContext, componentContainer) {
    this.expression = expression;
    this.componentContext = componentContext;
    this.componentContainer = componentContainer;
}
PropertyExpression.prototype.get = function(refresh) {
    if (refresh === true || this.value === void 0) {
        return this.value = parsePropertyExpression(this.expression, this.componentContext, this.componentContainer);
    }
    return this.value;
};

PropertyExpression.parse = parsePropertyExpression;
PropertyExpression.get = getPropertyComponentFromExpression;
PropertyExpression.isExpression = function(expression) {
    return stringExpression.test(expression);
};

module.exports = PropertyExpression;
