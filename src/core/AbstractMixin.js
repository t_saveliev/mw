var Parameter = require("Parameter");

//var Events = require("./EventsEmmiter");
//var inherits = require("inherits");
//inherits(AbstractMixin, Events);

var _ = require("lodash");
_.extend(AbstractMixin.prototype, require("./Events"));
function AbstractMixin(parameters) {
    parameters || (parameters = {});

    var parametersConfig = this.parameters,
        pName;

    // init declare parameters
    this._inputParameters = {};
    for (pName in parametersConfig) {
        Parameter.init(this, pName, parameters[pName], parametersConfig[pName]);
    }

    this.triggerMethod("init");
}

AbstractMixin.prototype.destruct = function() {
    this.stopListening();
    this.triggerMethod("destroy");
};
AbstractMixin.prototype.triggerMethod = function(event) {

    function getEventName(match, prefix, eventName) {
        return eventName.toUpperCase();
    }

    var splitter = /(^|[:-])(\w)/gi,
        methodName = "on" + event.replace(splitter, getEventName),
        method = this[methodName];

    if (typeof this.trigger === "function") {
        this.trigger.apply(this, arguments);
    }

    if (typeof method === "function") {
        return method.apply(this, slice.call(arguments, 1));
    }
};

AbstractMixin.prototype._setupRender = function() {};
AbstractMixin.prototype._beforeRender = function() {};
AbstractMixin.prototype._beforeRenderBody = function() {};
AbstractMixin.prototype._afterRenderBody = function() {};
AbstractMixin.prototype._afterRender = function() {};
AbstractMixin.prototype._cleanupRender = function() {};

module.exports = AbstractMixin;