var PropertyExpression = require("PropertyExpression");

function Parameter(options) {}

Parameter.init = function(ctx, name, value, options) {
    var _defaults = options.defaults,
        Type;

    if (value === void 0) {
        if (_defaults !== void 0) {
            value = (typeof _defaults === "string" && PropertyExpression.isExpression(_defaults)) ?
                PropertyExpression.parse(_defaults, ctx)
                : _defaults;

            Type = options.type;
            ctx[name] = Type ? new Type(value) : value;

        } else {
            if (options.required) {
                throw new Error("the __" + name + "__ of the required field");
            }
        }
    } else {
        if (typeof value === "string" && PropertyExpression.isExpression(value)) {
            ctx._inputParameters[name] = value;
            ctx[name] = PropertyExpression.parse(value, ctx.__componentContext, ctx.__componentContainer);
        } else {
            Type = options.type;
            ctx[name] = (options.create && Type) ? new Type(value) : value;
        }
    }
};

Parameter.read = function(ctx, name) {
    var inputValue = ctx._inputParameters[name];

    if (typeof inputValue === "string" && PropertyExpression.isExpression(inputValue)) {
        return ctx[name] = PropertyExpression.parse(inputValue, ctx.__componentContext, ctx.__componentContainer);
    } else {
        return ctx[name];
    }
};

module.exports = Parameter;