var Core = require("Core");
var AbstractComponent = require("AbstractComponent");
var Block = require("Block");

module.exports = Core.createClass("info", AbstractComponent, {

    __template: require("./info.tpl.xml"),

    parameters: {

        source: {required: true},

        value: {required: true},

        index: {defaults: "hello"},

        onChange: {defaults: "${prop:onChange}"},

        block: {type: Block},

        model: {}

    },

    _setupRender: function(MW) {
        MW.writeText("test-parameters -> " + this.onChange());
        MW.write("test");
    },

    _cleanupRender: function(MW) {
        MW.end();
    },

    onChange: function() {
        return this.value.get() + " - " + this.model.id;
    }

});
