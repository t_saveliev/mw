var Core = require("Core");
var AbstractComponent = require("AbstractComponent");
var TodoStore = require('../../stores/TodoStore');

module.exports = Core.createClass("todo-app", AbstractComponent, {

    __template: require("./todo-app.tpl.xml"),

    _init: function() {
        this.allTodos = TodoStore.getAll();
        this.areAllComplete = TodoStore.areAllComplete();

        TodoStore.addChangeListener(this._onChange.bind(this));
    },

    _destroy: function() {
        TodoStore.removeChangeListener(this._onChange.bind(this));
    },

    _onChange: function() {
        var zone = this.findEmbeddedResource("zone");
        zone.updateZone();
    }

});