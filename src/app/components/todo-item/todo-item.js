var Core = require("Core");
var AbstractComponent = require("AbstractComponent");
var TodoActions = require('../../actions/TodoActions');

module.exports = Core.createClass("todo-item", AbstractComponent, {

    __template: require("./todo-item.tpl.xml"),

    parameters: {
        todo: {required: true},
        key: {required: true}
    },

    _init: function() {
        this.key = this.key.get();
        this.todo = this.todo.get();
        this.isEditing = false;
    },

    onClickFromDestroy: function() {
        TodoActions.destroy(this.key);
    },

    onToggleFromComplete: function() {
        TodoActions.toggleComplete(this.todo);
    }

});