var Core = require("Core");
var AbstractComponent = require("AbstractComponent");
var TodoActions = require('../../actions/TodoActions');

module.exports = Core.createClass("footer", AbstractComponent, {

    __template: require("./footer.tpl.xml"),

    parameters: {
        allTodos: {required: true}
    },

    _init: function() {
        var allTodos = this.allTodos;
        var total = Object.keys(allTodos).length;

        var completed = 0;

        for (var key in allTodos) {
            if (allTodos[key].complete) {
                completed++;
            }
        }
        var itemsLeft = total - completed;

        this.completed = completed;
        this.itemsLeft = itemsLeft;
        this.itemsLeftPhrase = itemsLeft === 1 ? "item" : "items";
    },

    onClickFromClearCompleted: function() {
        TodoActions.destroyCompleted();
    }

});