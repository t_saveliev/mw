var Core = require("Core");
var AbstractComponent = require("AbstractComponent");
var Property = require("Property");
var _ = require("lodash");

var count = 0,
    dec = true;

module.exports = Core.createClass("test", AbstractComponent, {

    __template: require("./test.tpl.xml"),

    collection: ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"],

    model: {
        id: count,
        name: "model",
        info: "info info"
    },

    parameters: {
        title: {defaults: "WTML"},

        description: {defaults: "Язык разметки шаблонов"},

        collection: {defaults: "${collection}"},

        model: {defaults: "${model}"},

        value: {type: Property, defaults: null}
    },

    _init: function() {
        this.model.id = count++;
    },

    blockA: function() {
        return this.getBlock("blockA");
    },

    toUpperCaseValue: function() {
        return this.value.toString().toUpperCase();
    },

    isEvenGlobal: function() {
        return count % 2 === 0;
    },

    isEven: function(val) {
        return val % 2 === 0;
    },

    getItemCls: function(val) {
        return "item_" + (this.isEven(val) ? "even" : "odd");
    },

    onChangeSelect: function(select, val) {
        this.model.info = "info " + val;
        this.value.set(val);

        _.each(this.findEmbeddedResource("info"), function(component) {
            component.trigger("change:model");
        });
    },

    onClickButtonFromEventTest: function(button) {
        var zone = this.findEmbeddedResource("zone");

        this.sortCollection();

        count++;

        //zone.updateZone(this.getBlock("blockA"));
        zone.updateZone();

        button.update();
        //button.setAttr("disabled", true);

        console.log("click", zone);
    },

    sortCollection: function() {
        dec = !dec;

        this.collection.sort(function(a, b) {
            var res;

            if (a < b) {
                res = -1;
            } else if (a > b) {
                res = 1;
            } else {
                res = 0;
            }

            return dec ? res : -res;
        });

        this.model.id = count;

        this.value.set("A");
    }

/*
    function(blocks, MB) {

        blocks.blockA = [
            MB.create("info", {
                resourceId: "info",
                source: "${collection}",
                value: "${value}",
                model: "${model}"
            }, [
                " - ${value}",
                MB.create("hr")
            ])
        ];

        blocks.main = [
            MB.create("page", {
                resourceId: "page",
                title: "${title}",
                description: "${description}"
            }, [
                MB.create("elem", {tag: "header", class: "header"}, [
                    MB.create("div", {class: "header__res1", resourceId: "header"}, "Resource header 1"),
                    MB.create("div", {class: "header__res2", resourceId: "header"},
                        MB.create("button", {class: "button", resourceId: "event-test", id: "${model.id}"}, "Resource header ${model.id}")
                    )
                ]),
                MB.create("zone", {class: "container", resourceId: "zone"}, [
                    "model.info = ",
                    MB.create("text", {text: "${model.info}"})
                        .mixin("bind", {update: "change:select"}),
                    MB.create("br"),
                    MB.create("input", {
                        value: "${model.id}"
                    }),
                    MB.create("br"),
                    MB.create("loop", {
                        source: "${collection}", value: "${value}", index: "${var:index}"
                    }, [
                        MB.renderBlock("${block:blockA}"),
                        //MB.create("delegate", {to: "${block:blockA}"}),
                        MB.create("div", {
                            class: "item ${getItemCls(var:index)}"
                        }, [
                            "============ ${toUpperCaseValue()} ============ ",
                            MB.create("if", {
                                    test: "${isEven(var:index)}"
                                },
                                "true"
                            ).parameter("else",
                                "false"
                            ),
                            MB.create("if", {
                                test: "${getItemCls(var:index)}"
                            }, [
                                MB.create("div", null, [
                                    MB.create("span", {class: "value value_${value}"},
                                        "${model.info} - "
                                    ).mixin("bind", {
                                        attr: {class: "change:select"},
                                        body: "change:select"
                                    }),
                                    MB.create("select", {class: "select", source: "${collection}", value: "${value}"})
                                ])
                            ]).parameter("else", [
                                MB.create("div", null, "${var:index}")
                            ])
                        ])
                    ]),
                    MB.create("br"),
                    MB.create("if", {
                        test: "${isEvenGlobal()}"
                    }, [
                        "Нечётно"
                    ]).parameter("else", [
                        "Чётно"
                    ])
                ])
            ])
        ];

    }
*/

});
