var Core = require("Core");
var AbstractComponent = require("AbstractComponent");
var TodoActions = require('../../actions/TodoActions');

module.exports = Core.createClass("header", AbstractComponent, {

    __template: require("./header.tpl.xml"),

    onSaveFromNewTodo: function(componentInput, text) {

        componentInput.setValue("");

        if (text.trim()){
            TodoActions.create(text);
        }
    }

});