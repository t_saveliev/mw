var createClass = require("createClass");
var AbstractComponent = require("AbstractComponent");
var _ = require("lodash");

module.exports = createClass("loop", AbstractComponent, {

    parameters: {
        source: {required: true},
        index: {},
        key: {},
        value: {}
    },

    _setupRender: function() {
        var keys = _.keys(this.source),
            len = keys.length;

        if (len === 0) return false;

        this._i = 0;
        this.keys = keys;
        this.sourceLength = len - 1;
    },

    _beforeRender: function() {
        var index = this._i,
            key = this.keys[index];

        this.index && this.index.set(index);
        this.key && this.key.set(key);
        this.value && this.value.set(this.source[key]);
    },

    _afterRender: function() {
        if (this._i < this.sourceLength) {
            this._i++;

            return false;
        }
    }
});
