var Core = require("Core");
var AbstractComponent = require("AbstractComponent");

module.exports = Core.createClass("page", AbstractComponent, {

    __template: require("./page.tpl.xml"),

    parameters: {
        title: {},
        description: {}
    }

});