var Core = require("Core");
var AbstractComponent = require("AbstractComponent");
var TodoActions = require('../../actions/TodoActions');

module.exports = Core.createClass("section", AbstractComponent, {

    __template: require("./section.tpl.xml"),

    parameters: {
        allTodos: {required: true},
        areAllComplete: {required: true}
    },

    onToggleFromToggleAll: function() {
        TodoActions.toggleCompleteAll();
    }

});