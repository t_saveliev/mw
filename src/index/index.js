var $ = require("jquery");
var Benchmark = require("benchmark");
var TestComponent = require("../../components/test/test");

window._ = require("lodash");

var testData = window.testData = (function(data) {
    for(var i = 0, len = 100000; i < len; i++) {
        data.push({
            number: i
        });
    }

    return data;
})([]);
var testObjectData = window.testObject = (function(data) {
    for(var i = 0, len = testData.length; i < len; i++) {
        data[i] = testData[i];
    }

    return data;
})({});

function testArray() {
    var mem = [];
    for(var i = 0, len = testData.length; i < len; i++) {
        mem.push(testData[i]);
    }
}
function testObject() {
    var mem = {};
    for(var i = 0, len = testData.length; i < len; i++) {
        mem[i] = testData[i];
    }
}

function testArrayRead() {
    for(var i = 0, len = testData.length; i < len; i++) {
        testData[i];
    }
}
function testObjectRead() {
    for(var i = 0, len = testData.length; i < len; i++) {
        testObjectData[i];
    }
}

function runTest1() {
    var suite = new Benchmark.Suite;

    suite
        .add('testArray', testArray)
        .add('testObject', testObject)
        .on('complete', function() {
            _.each(this, function(target) {
                console.log(String(target));
            });
            console.log('Fastest is ' + this.filter('fastest').pluck('name'));
        })
        .run({async: true});
}
function runTest2() {
    var suite = new Benchmark.Suite;

    suite
        .add('testObject', function() {
            var component = new TestComponent(null, null);
            component.render();
        })
        .on('complete', function() {
            _.each(this, function(target) {
                console.log(String(target));
            });
            console.log('Fastest is ' + this.filter('fastest').pluck('name'));
        })
        .run({async: true});
}

function runTest3() {
    var suite = new Benchmark.Suite,
        string = "Hello world";

    suite
        .add('testTypeof', function() {
            return (typeof string === "string") ? string : string.toString();
        })
        .add('testToString', function() {
            return string.toString();
        })
        .on('complete', function() {
            _.each(this, function(target) {
                console.log(String(target));
            });
            console.log('Fastest is ' + this.filter('fastest').pluck('name'));
        })
        .run({async: true});
}

$(function() {
    var $zone = $("#zone");

    var component = new TestComponent(null, null);
    $zone.html(component.render());

    console.log(component);

    $(document)
        .on("click", "button#test1", function() {
            console.log("start test");
            runTest3();
        })
        .on("click", "button#test2", function() {
            console.log("start test");
            runTest2();
        })
        .on("click", "button#dom", function() {

            var containerElement = document.createElement("div");

            $zone.empty();

            for (var i = 0; i < 100; i++) {
                var component = new TestComponent(null, null);
                containerElement.appendChild(component.render());
            }
            $zone.html(containerElement);

            console.log(document.querySelectorAll("*").length);
        });
});
