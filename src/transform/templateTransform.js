var expat = require('node-expat');
var defaultComponents = require('./defaultComponents');

var NAMESPACE_URI = "http://mb.org";
var NAMESPACE_REGEXP = /^xmlns:/;
var PREFIX_REGEXP = /(^\w+):(.+)/;
var BUNDLE_REGEXP = /([^/]+)\/components\/[^/]+\/[^/]+$/;
var COMPONENT_REGEXP = /^(?:(\w+)\.)?([^.]+$)/;

var depth,
    tNamespace,
    pNamespace,
    currentElement;

var isNewString = false,
    stringBuffer = "";

var dependencies = [];

function Root()  {
    this.buffer = [
        new Block("main", this)
    ];
}
Root.prototype.append = function(elem) {
    if (elem instanceof Block) {
        this.buffer.push(elem);
    } else {
        this.buffer[0].append(elem);
    }
};
Root.prototype.toString = function() {
    var buffer = this.buffer,
        _str = "";

    _str += "var Core = require(\"Core\");\n";

    _str += buildComponentDependencies();

    _str += "module.exports = Core.buildTemplate(function(blocks, MB) {\n";

    _str += buffer.map(function(block) {
        return block.toString();
    }).join("");

    _str += "});";

    return _str;
};

function Block(name, parent)  {
    this.name = name;
    this.parent = parent;
}
Block.prototype.append = function(elem) {
    (this.children || (this.children = [])).push(elem);
};
Block.prototype.toString = function() {
    var name = this.name,
        children = this.children,
        _str;

    depth++;
    _str = writeIndent(depth) + "blocks." + name + " = [";

    if (children) {
        depth++;
        _str += children.map(function(item) {
            return item.toString();
        }).join(", ");
        depth--;
    }
    _str += "\n" + writeIndent(depth) + "];\n";
    depth--;

    return _str;
};

function Elem(name, attrs, parent, local) {
    this.name = name;
    this.attrs = attrs || {};
    this.parent = parent;
    this.local = local === true || false;
}
Elem.prototype.append = function(elem) {
    (this.children || (this.children = [])).push(elem);
};
Elem.prototype.appendChain = function(elem) {
    (this.chain || (this.chain = [])).push(elem);
};
Elem.prototype.toString = function() {
    var name = this.name,
        attrs = this.attrs,
        children = this.children,
        chain = this.chain,
        local = this.local,

        hasAttributes = !isEmpty(attrs),
        _str;

    _str = "\n" + writeIndent(depth) + "MB.create(";

    if (local) {
        _str += "imports[" + dependencies.indexOf(name) + "]";
    } else {
        _str += "\"" + name + "\"";
    }

    if (hasAttributes) {
        _str += ", " + JSON.stringify(attrs);
    }

    if (children) {
        depth++;
        _str += (hasAttributes) ? ", " : ", null, ";
        _str += "[" + children.map(function(item) {
            return item.toString();
        }).join(", ");
        depth--;
        _str += "\n" + writeIndent(depth) + "]";
    }

    _str += ")";

    if (chain) {
        _str += chain.map(function(item) {
            return item.toString();
        });
    }

    return _str;
};

function Text(text) {
    this.text = text;
}
Text.prototype.toString = function() {
    var _str = this.text;

    return "\n" + writeIndent(depth) + "\"" +  regExpEscape(_str) + "\"";
};

function Command(text) {
    this.text = text;
}
Command.prototype.toString = function() {
    return "\n" + writeIndent(depth) + "MB." + this.text;
};

function Chain(name, value, parent) {
    this.name = name;
    this.value = value;
    this.parent = parent;
}
Chain.prototype.append = function(elem) {
    (this.children || (this.children = [])).push(elem);
};
Chain.prototype.toString = function() {
    var name = this.name,
        value = this.value,
        children = this.children,
        _str;

    _str = "." + name + "(\"" + value + "\"";

    if (children) {
        depth++;
        if (children.length === 1) {
            _str += ", " + children[0].toString() + "\n";
        } else {
            _str += ", [" + children.map(function(item) {
                return item.toString();
            }).join(", ");
            _str += "\n" + writeIndent(depth) + "]";
        }
        depth--;
        _str += writeIndent(depth);
    }

    _str += ")";

    return _str;
};

function Mixin(name, parent) {
    this.name = name.replace(/^mixin./, "");
    this.parent = parent;
}
Mixin.prototype.append = function(elem) {
    this.children = elem;
};
Mixin.prototype.toString = function() {
    var name = this.name,
        children = this.children,
        _str;

    _str = ".mixin" + "(\"" + name + "\"";

    if (children) {
        depth++;
        _str += ", " + children;
        depth--;
    }

    _str += ")";

    return _str;
};

function isEmpty(obj) {
    var el;

    for (el in obj) {
        return false;
    }

    return true;
}
function writeIndent(depth) {
    var i = 0,
        _str = "";

    while (i++ < depth) {
        _str += "  ";
    }

    return _str;
}
function regExpEscape(str) {
    return String(str).replace(/[\"\']/g, '\\$&').replace(/\n/g, '\\n');
}

function stringBuilderPush(text) {
    if (text === '\n') {
        isNewString = true;
    } else {
        if (isNewString) {
            text = text.trim();

            if (text !== "" && stringBuffer !== "") {
                stringBuffer += "\n";
            }
        }
        stringBuffer += text;
        isNewString = false;
    }
}
function stringBuilderWrite() {
    var _res;

    isNewString = false;

    if (stringBuffer === "") return false;

    _res = stringBuffer;
    onText(_res);
    stringBuffer = "";
}

function getNamespaceKey(namespaces, value) {
    for (var name in namespaces) {
        if (namespaces[name] === value) return name;
    }
    return false;
}
function initNamespaces(namespaces) {
    if (namespaces.length === 0 || (tNamespace = getNamespaceKey(namespaces, NAMESPACE_URI)) === false) {
        throw new Error("Not valid document");
    }

    pNamespace = getNamespaceKey(namespaces, tNamespace + ":parameter");
}
function determineCurrentBundle(filename) {
    var match = BUNDLE_REGEXP.exec(filename);
    currentBundle = match && match[1] || null;
}

function getUrlComponentDependencies(component) {
    var match = COMPONENT_REGEXP.exec(component),
        bundle = match[1] || currentBundle,
        name = match[2],
        url = "";

    if (bundle) {
        url += bundle + "/components/";
    }

    url += name + "/" + name;

    return url;
}
function buildComponentDependencies() {
    var str = "",
        len = dependencies.length;

    if (len) {
        var deps = [],
            i = 0;

        for (; i < len; i++) {
            var component = dependencies[i];
            deps.push(writeIndent(1) + "require(\"" + getUrlComponentDependencies(component) + "\")");
        }

        str = "var imports = [\n" + deps.join(",\n") + "\n];\n";
    }

    return str;
}

function onBegin() {
    depth = 0;
    tNamespace = null;
    pNamespace = null;
    currentElement = new Root();
    dependencies = [];
}
function onClose() {
    return currentElement.toString();
}
function onStartElement(elem, attrs, prefix, namespaces) {
    stringBuilderWrite();

    var _elem;

    // init namespaces
    if (tNamespace === null && currentElement instanceof Root) initNamespaces(namespaces);

    // if parameter
    if (prefix === pNamespace) {
        if (currentElement instanceof Chain || currentElement instanceof Mixin) {
            throw new Error("Not true design parameters");
        }

        _elem = /^mixin/.test(elem) ?
            new Mixin(elem, currentElement) :
            new Chain("parameter", elem, currentElement);

        currentElement.appendChain(_elem);
    }

    // if component
    else {
        if (prefix === tNamespace) {
            switch (elem) {
                case "block" :
                    _elem = new Block(attrs["id"], currentElement);
                    break;

                case "container" :
                    return;
                /*
                 case "body" :
                 _elem = new Command("renderBody()");
                 break;

                 case "delegate" :
                 _elem = new Command("renderBlock(\"" + attrs["to"] + "\")");
                 break;
                 */

                default :
                    if (!defaultComponents[elem]) {
                        if (dependencies.indexOf(elem) === -1) {
                            dependencies.push(elem);
                        }
                        _elem = new Elem(elem, attrs, currentElement, true);
                    } else {
                        _elem = new Elem(elem, attrs, currentElement);
                    }
            }
        } else {
            _elem = new Elem(elem, attrs, currentElement);
        }

        currentElement.append(_elem);
    }

    currentElement = _elem;

}
function onEndElement() {
    stringBuilderWrite();
    currentElement = currentElement.parent || currentElement;
}
function onText(text) {
    currentElement.append(currentElement instanceof Mixin ? text : new Text(text));
}

var parser = new expat.createParser();
parser.on('startElement', function(name, attrs) {

    var namespaces = {},
        prefix = null,
        matches,
        attr;

    for (attr in attrs) {
        if (NAMESPACE_REGEXP.test(attr)) {
            namespaces[attr.replace(NAMESPACE_REGEXP, "")] = attrs[attr];
            delete attrs[attr];
        }
    }

    if (matches = PREFIX_REGEXP.exec(name)) {
        prefix = matches[1];
        name = matches[2];
    }

    onStartElement(name, attrs, prefix, namespaces);
});
parser.on('endElement', function() {
    onEndElement();
});
parser.on('text', function (text) {
    stringBuilderPush(text);
});

module.exports = function templateTransform(data, opt) {
    var result;

    opt || (opt = {});
    determineCurrentBundle(opt.filename);
    onBegin();
    result = parser.parse(data);
    parser.reset();
    if (result === false) console.log(parser.getError());
    result = onClose();

    return result;
};