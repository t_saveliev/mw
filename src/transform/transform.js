var through = require("through");
var gutil = require("gulp-util");
var utils = require("../core/utils");
var templateTransform = require("./templateTransform");

var DEFAULT_EXT = "tpl.xml";

module.exports = function(opt) {

    opt || (opt = {});

    var fileExtensionRegExp = new RegExp(utils.regExpEscape("." + (opt.ext || DEFAULT_EXT)) + "$");

    return function(file) {
        if (!fileExtensionRegExp.test(file)) return through();

        var data = "";

        function write(buf) { data += buf; }

        function end() {
            gutil.log("Building template", gutil.colors.green(file));

            this.queue(templateTransform(data, {filename: file}));
            this.queue(null);
        }

        return through(write, end);
    };

};