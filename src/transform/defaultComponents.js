var libs = {
    "any"       : "corelib/components/Any",
    "body"      : "corelib/components/Body",
    "button"    : "corelib/components/Button",
    "delegate"  : "corelib/components/Delegate",
    "if"        : "corelib/components/If",
    "input"     : "corelib/components/Input",
    "loop"      : "corelib/components/Loop",
    "select"    : "corelib/components/Select",
    "text"      : "corelib/components/Text",
    "zone"      : "corelib/components/Zone"
};

module.exports = libs;