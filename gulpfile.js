var gulp = require("gulp");
var bundles = require("./gulp/tasks/bundles");

gulp.task("bundles", bundles("./src/app"));

gulp.task("watch", function () {
    gulp.watch(["src/**/*.html", "src/**/*.js", "src/**/*.tpl.xml"], ["bundles"]);
});

gulp.task("start", ["bundles", "watch"]);
