var inherits = require('inherits');
var EventEmitter = require('events').EventEmitter;
var util = require("utils");

EventEmitter.defaultMaxListeners = 100;

inherits(Events, EventEmitter);
function Events() {}

Events.prototype.trigger = EventEmitter.prototype.emit;

Events.prototype.off = function(name, callback) {

    name ? this.removeListener(name, callback) : this.removeAllListeners();

    return this;
};

Events.prototype.stopListening = function(obj, name, callback) {
    var listeners = this._listeners;
    if (!listeners) return this;
    var deleteListener = !name && !callback;
    if (typeof name === "object") callback = this;
    if (obj) (listeners = {})[obj._listenerId] = obj;
    for (var id in listeners) {
        listeners[id].off(name, callback);
        if (deleteListener) delete this._listeners[id];
    }
    return this;
};

(function() {
    var listenMethods = {
            listenTo: 'on',
            listenToOnce: 'once'
        },
        method;

    for (method in listenMethods) {
        (function(implementation, method) {
            Events.prototype[method] = function(obj, name, callback) {
                var listeners = this._listeners || (this._listeners = {}),
                    id = obj._listenerId || (obj._listenerId = util.uniqueId('l'));

                listeners[id] = obj;

                if (typeof name === "object") callback = this;

                obj[implementation](name, callback.bind(this));

                return this;
            };
        })(listenMethods[method], method);
    }
})();

module.exports = Events;
