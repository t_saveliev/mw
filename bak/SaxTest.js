var expat = require('node-expat');
var parser = new expat.createParser();

var isNewString = false,
    stringBuffer = "";

var NAMESPACE_REGEXP = /^xmlns:/;

function stringBuilderPush(text) {
    if (text === '\n') {
        isNewString = true;
    } else {
        if (isNewString) {
            text = text.trim();

            if (text !== "" && stringBuffer !== "") {
                stringBuffer += "\n";
            }
        }
        stringBuffer += text;
        isNewString = false;
    }
}
function stringBuilderWrite() {
    var _res;

    isNewString = false;

    if (stringBuffer === "") return false;

    _res = stringBuffer;
    stringBuffer = "";
    console.log(_res);

    return _res;
}
function stringBuilderReset() {
    isNewString = false;
    stringBuffer = "";
}

parser.on('startElement', function (name, attrs) {
    var namespaces = {},
        prefix = null,
        matches,
        attr;

    for (attr in attrs) {
        if (NAMESPACE_REGEXP.test(attr)) {
            namespaces[attr.replace(NAMESPACE_REGEXP, "")] = attrs[attr];
            delete attrs[attr];
        }
    }

    if (matches = /(^\w+):(.+)/.exec(name)) {
        prefix = matches[1];
        name = matches[2];
    }

    console.log(">>>>", name, attrs, prefix, namespaces);
    stringBuilderWrite();
});

parser.on('endElement', function () {
    stringBuilderWrite();
});

parser.on('text', function (text) {
    stringBuilderPush(text);
});

parser.on('error', function (error) {
    console.log("ERROR", error);
});

var through = require('through');

module.exports = function(file) {
    if (!/\.tpl\.xml$/.test(file)) return through();

    var data = "";

    function write(buf) {
        data += buf;
    }

    function end() {
        var res;

        this.queue(res = parser.parse(data));
        if (res === false) console.log(parser.getError());
        parser.reset();

        this.queue(null);
    }

    return through(write, end);
};
