function ComponentResources() {
    this.resources = [];
}
ComponentResources.prototype.add = function(component) {
    this.resources.push(component);
};
ComponentResources.prototype.del = function(component) {
    var resources = this.resources,
        index = resources.indexOf(component);

    if (index !== -1) {
        resources[index] = null;

        return true;
    }

    return false;
};
ComponentResources.prototype.clear = function() {
    var resources = this.resources;

    var len = resources.length,
        i = 0;

    for(; i < len; i++ ) {
        resources[i].destruct();
    }
};
ComponentResources.prototype.compress = function() {
    var resources = this.resources,
        _resources = [],
        res;

    var len = resources.length,
        i = 0;

    for(; i < len; i++ ) {
        if (null !== (res = resources[i])) {
            _resources.push(res);
        }
    }

    this.resources = _resources;
};
